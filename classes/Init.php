<?php 
namespace Pluggingg;
use Pluggingg\Domain\Clients;
use Pluggingg\Domain\Ticket;
use Pluggingg\Helpers\SearchHelpers;
use Pluggingg\Templater;

class Init
{
    private $temp;
    private $clients;

    public function __construct() {
        $this->temp = new Templater();
        add_action( 'init', [ $this, 'create_posttypes'] );
        add_shortcode('pluggingg_client_list',  [ $this, 'pluggingg_clients_listing']);
        add_shortcode('pluggingg_one_client_tickets_listing',  [ $this, 'pluggingg_one_client_tickets_listing']);

        add_shortcode('pluggingg_ticket_add',  [ $this, 'pluggingg_ticket_add']);
        add_action('admin_post_pluggingg_add_post',  [ $this, 'pluggingg_add_post']);
    }

    public function create_posttypes(){
        $this->clients = new Clients();
        new Ticket();
    }

    public function pluggingg_clients_listing(){
        $list = $this->clients->get_all_clients();
        ob_start();
        if($list->have_posts()){
            while($list->have_posts()){
                $list->the_post();
                $this->temp->pluggingg_get_template("pluggingg-list.php");
            }
        }
        
        $content = ob_get_contents();
        ob_get_clean();

        return $content;
    }

    public function pluggingg_one_client_tickets_listing(){
        return $this->temp->pluggingg_get_template("tickets-list.php");
    }

    public function pluggingg_ticket_add(){
        return $this->temp->pluggingg_get_template("ticket-post.php");
    }

    public function pluggingg_add_post(){

        $email = sanitize_text_field($_POST['courriel']);
        $nom = sanitize_text_field($_POST['nom']);

        $check_email = $this->clients->check_if_email_exist($email);
    
        if (empty( $check_email )) {

            $nouveau_client = [
                'post_title' => $nom,
                'post_status' => "publish", 
                'post_date' => date('Y-m-d'),
                'post_type' => "pluggingg_clients",
            ];
            
            $post_id_client = wp_insert_post($nouveau_client);
            update_post_meta($post_id_client, "pluggingg_email",$email);
            
            $name_client = $nom;

        } else {
            foreach($check_email as $data){
                $name_client = $data->post_title;
            }
        }

        $post_commentaire = sanitize_text_field($_POST['commentaire']);
        $post_commentaire_trim = wp_trim_words($post_commentaire, 6, '...');
        
        $nouveau_ticket = [
            'post_title' => $post_commentaire_trim,
            'post_content' => $post_commentaire, 
            'post_status' => "publish", 
            'post_date' => date('Y-m-d'),
            'post_type' => "pluggingg_tickets",
        ];

        $post_id_ticket = wp_insert_post($nouveau_ticket);
        update_post_meta($post_id_ticket, "pluggingg_lien_client", $name_client );

        wp_redirect("/ticketClient/?nom=" . $name_client);

    }
}
