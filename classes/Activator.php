<?php
namespace Pluggingg;
use Pluggingg\Init;

class Activator
{
    function Pluggingg_init() { 
        new Init();
        flush_rewrite_rules();
    }
}
