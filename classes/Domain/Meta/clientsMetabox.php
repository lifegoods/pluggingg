<?php

namespace Pluggingg\Domain\Meta;

use Pluggingg\Templater;

class ClientsMetabox {
    public $field;
    public function __construct()
    {
        $this->field = 'pluggingg_email';
        $this->temp = new Templater();
        add_action("add_meta_boxes", [$this, "add_clients_metaboxes"]);
        add_action("save_post_pluggingg_clients", [$this, "save_clients_metaboxes"]);
    }

    public function add_clients_metaboxes()
    {
        add_meta_box('_pluggingg_email', __('Votre courriel', 'pluggingg'), [$this, 'email_metabox_callback'], "pluggingg_clients", "normal");
    }

    public function email_metabox_callback()
    {
        echo $this->temp->pluggingg_get_template("metaboxes/email_client.php");
    }

    public function save_clients_metaboxes($post_id)
    {
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;
        if ($parent_id = wp_is_post_revision($post_id)) {
            $post_id = $parent_id;
        }

        if (array_key_exists($this->field, $_POST)) {
            update_post_meta($post_id, $this->field, sanitize_text_field($_POST[$this->field]));
        }
    }
}
