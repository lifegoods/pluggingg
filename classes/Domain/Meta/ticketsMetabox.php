<?php

namespace Pluggingg\Domain\Meta;

use Pluggingg\Templater;

class TicketsMetabox {
    public $field;
    public function __construct() {
        $this->field = 'pluggingg_lien_client';
        $this->temp = new Templater();
        add_action("add_meta_boxes", [$this, "add_tickets_metaboxes"]);
        add_action("save_post_pluggingg_tickets", [$this, "save_tickets_metaboxes"]);
    }

    public function add_tickets_metaboxes()
    {
        add_meta_box('_pluggingg_lien_client', __('Sélectionner un client', 'pluggingg'), [$this, 'lien_client_metabox_callback'], "pluggingg_tickets", "normal");
    }

    public function lien_client_metabox_callback() {
        echo $this->temp->pluggingg_get_template("metaboxes/selectionner_client.php");
    }

    public function save_tickets_metaboxes($post_id) {
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;
        if ($parent_id = wp_is_post_revision($post_id)) {
            $post_id = $parent_id;
        }

        if (array_key_exists($this->field, $_POST)) {
            update_post_meta($post_id, $this->field, sanitize_text_field($_POST[$this->field]));
        }
    }
}
