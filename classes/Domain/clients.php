<?php
namespace Pluggingg\Domain;

use Pluggingg\Domain\Meta\ClientsMetabox;
use WP_Query;
class Clients extends ClientsMetabox{
  
  public function __construct () {
    parent::__construct();
    $this->create_clients_post_type();
    $this->create_client_taxonomy();
  }

  public function create_clients_post_type() {
    $label = array (
        'name'               => __ ( 'Clients', 'pluggingg' ),
        'singular_name'      => __ ( 'Client', 'pluggingg' ),
        'menu_name'          => _x ( 'Clients', 'Admin menu name', 'pluggingg' ),
        'add_new'            => __ ( 'Ajouter une client', 'pluggingg' ),
        'add_new_item'       => __ ( 'Ajouter une client', 'pluggingg' ),
        'edit'               => __ ( 'Editer la client', 'pluggingg' ),
        'edit_item'          => __ ( 'Editer la clients', 'pluggingg' ),
        'new_item'           => __ ( 'Nouvelle client', 'pluggingg' ),
        'view'               => __ ( 'Voir la client', 'pluggingg' ),
        'view_item'          => __ ( 'Voir la client', 'pluggingg' ),
        'search_items'       => __ ( 'Rechercher une client', 'pluggingg' ),
        'not_found'          => __ ( 'Aucun client trouvée', 'pluggingg' ),
        'not_found_in_trash' => __ ( 'Aucune client trouvée dans la corbeille', 'pluggingg' ),
        'parent'             => __ ( 'Client parent', 'pluggingg' ),
    );

    $args = array (
        'labels'              => $label,
        'public'              => true,
        'has_archive'         => true,
        'publicly_queryable'  => true,
        'exclude_from_search' => false,
        'show_in_menu'        => true,
        'hierarchical'        => false,
        'supports'            => ["title"],
        'show_in_rest'        => true,
        'menu_icon'           => 'dashicons-id-alt', 
        'rest_base'         => 'pluggingg_clients',
        'rewrite'             => array ( 'slug' => 'clients' ),
        'capability_type'     => 'post',
    );
    register_post_type( 'pluggingg_clients', $args );
  }
  
  public function create_client_taxonomy() {
    $labels = array(
        'name'              => _x( 'Catégorie de client', 'taxonomy general name', 'pluggingg' ),
        'singular_name'     => _x( 'Catégorie de clients', 'taxonomy singular name', 'pluggingg' ),
        'search_items'      => __( 'Recherche catégorie de client', 'pluggingg' ),
        'all_items'         => __( 'Tout les catégories de clients', 'pluggingg' ),
        'parent_item'       => __( 'Catégories de clients parent', 'pluggingg' ),
        'parent_item_colon' => __( 'Catégories de clients parent:', 'pluggingg' ),
        'edit_item'         => __( 'Editer le catégorie de clients', 'pluggingg' ),
        'update_item'       => __( 'Mettre a jour le catégorie de clients', 'pluggingg' ),
        'add_new_item'      => __( 'Ajouter une nouvelle catégorie de clients', 'pluggingg' ),
        'new_item_name'     => __( 'Nouvelle catégorie de clients', 'pluggingg' ),
        'menu_name'         => __( 'Catégorie de clients', 'pluggingg' ),
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'show_in_menu'      => true,
        'show_in_nav_menus' => true,  
        'query_var'         => true,
        'show_in_rest'      => true,
        'rest_base'         => 'clients_categories',
        'rewrite'           => array( 'slug' => 'Catégorie/clients' ),
    );

    register_taxonomy( 'client_categorie', 'pluggingg_clients', $args );
  }

  static public function get_all_clients() {
    $args = array(
        "post_type" => "pluggingg_clients",
        'order' => 'ASC',
        'orderby'   => 'date',
        "posts_per_page" => -1
    );
    
    return new WP_Query($args);
  }

  public function check_if_email_exist($email_to_search){
      $args = array(
          'post_type' => 'pluggingg_clients',
          'meta_query' => array(
              array(
                  'key' => 'pluggingg_email',
                  'value' => $email_to_search
              )
          ),
        );

        $query = new WP_Query( $args );
        $post = $query->posts;

        return $post;
  }
}



