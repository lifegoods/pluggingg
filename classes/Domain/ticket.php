<?php
namespace Pluggingg\Domain;

use Pluggingg\Domain\Meta\TicketsMetabox;
use WP_Query;
class Ticket extends TicketsMetabox{
    public function __construct()
    {
        parent::__construct();
        $this->create_ticket_post_type();
        $this->create_ticket_taxonomy();
    }

    public function create_ticket_post_type() {
        $label = array(
            'name'               => __('Tickets', 'pluggingg'),
            'singular_name'      => __('Ticket', 'pluggingg'),
            'menu_name'          => _x('Tickets', 'Admin menu name', 'pluggingg'),
            'add_new'            => __('Ajouter une ticket', 'pluggingg'),
            'add_new_item'       => __('Ajouter une ticket', 'pluggingg'),
            'edit'               => __('Editer la ticket', 'pluggingg'),
            'edit_item'          => __('Editer la tickets', 'pluggingg'),
            'new_item'           => __('Nouvelle ticket', 'pluggingg'),
            'view'               => __('Voir la ticket', 'pluggingg'),
            'view_item'          => __('Voir la ticket', 'pluggingg'),
            'search_items'       => __('Rechercher une ticket', 'pluggingg'),
            'not_found'          => __('Aucun ticket trouvée', 'pluggingg'),
            'not_found_in_trash' => __('Aucune ticket trouvée dans la corbeille', 'pluggingg'),
            'parent'             => __('Ticket parente', 'pluggingg'),

        );
        $args = array(
            'labels'              => $label,
            'public'              => true,
            'has_archive'         => true,
            'publicly_queryable'  => true,
            'exclude_from_search' => false,
            'show_in_menu'        => true,
            'menu_icon'           => 'dashicons-tickets-alt',     
            'hierarchical'        => false,
            'supports'            => ["title","editor", "comments"],
            'rewrite'             => array('slug' => 'tickets'),
            'capability_type'     => 'post',
        );
        register_post_type('pluggingg_tickets', $args);
    }
  
    public function create_ticket_taxonomy() {
        $labels = array(
            'name'              => _x( 'Type de ticket', 'taxonomy general name', 'pluggingg' ),
            'singular_name'     => _x( 'Type de ticket', 'taxonomy singular name', 'pluggingg' ),
            'search_items'      => __( 'Recherche type de ticket', 'pluggingg' ),
            'all_items'         => __( 'Tout les types de ticket', 'pluggingg' ),
            'parent_item'       => __( 'Type de tickets parent', 'pluggingg' ),
            'parent_item_colon' => __( 'Type de tickets parent:', 'pluggingg' ),
            'edit_item'         => __( 'Editer le type de ticket', 'pluggingg' ),
            'update_item'       => __( 'Mettre a jour le type de ticket', 'pluggingg' ),
            'add_new_item'      => __( 'Ajouter un nouveau type de ticket', 'pluggingg' ),
            'new_item_name'     => __( 'Nouveau type de ticket', 'pluggingg' ),
            'menu_name'         => __( 'Types de ticket', 'pluggingg' ),
        );

        $args = array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'show_in_menu'      => true,
            'show_in_nav_menus' => true,
            'query_var'         => true,
            'show_in_rest'      => true,
            'rest_base'         => 'tickets_types',
            'rewrite'           => array( 'slug' => 'type/ticket' ),
        );

    register_taxonomy( 'ticket_type', 'pluggingg_tickets', $args );
    }
    
    static public function get_all_tickets() {
        $args = array(
            "post_type" => "pluggingg_clients",
            'order' => 'ASC',
            'orderby'   => 'date',
            "posts_per_page" => -1
        );
        
        return new WP_Query($args);
      }
    
      static public function get_all_tickets_one_client($client_name = '') {
          $args = array(
              "post_type" => "pluggingg_tickets",
              'order' => 'ASC',
              'orderby'   => 'date',
              'meta_query'=> array(
                  array(
                    'key' => 'pluggingg_lien_client',
                    'compare' => '=',
                    'value' => $client_name,
                  )
              ),
              "posts_per_page" => -1
          );
          
          return new WP_Query($args);
        }
}