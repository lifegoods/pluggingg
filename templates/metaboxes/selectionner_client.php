<?php
namespace Pluggingg\Domain;
use Pluggingg\Domain\Clients;

$this->temp = new Clients();
$list = $this->temp->get_all_clients();

$retreive_meta_client = get_post_meta($_GET['post'], "pluggingg_lien_client", true);

if($list->have_posts()){
    ?>
    <label for="pluggingg_lien_client">Client:</label>
    <select id="pluggingg_lien_client" name="pluggingg_lien_client" >
        <option value="">--Aucun--</option>
        <?php
        

           while($list->have_posts()){
               $list->the_post();
               ?>
                   <option value="<?php the_title(); ?>" 
                   <?php 
                    if ($retreive_meta_client == get_the_title()){
                        echo 'selected="selected"';
                    } else {
                        echo '';
                    }
                   
                        ?>
                    ><?php the_title(); ?></option>
               <?php
           }
        ?>
    </select>

<?php
}
?>





