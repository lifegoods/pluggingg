<?php 
 $email_client = get_post_meta(get_the_ID(), "pluggingg_email", true);
?>

<label for="pluggingg_email">Email</label>
<input id="pluggingg_email" type="email" name="pluggingg_email" value="<?php echo $email_client; ?>">
