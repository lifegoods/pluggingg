<form role="search" method="post" id="searchform" action="<?php echo admin_url('admin-post.php') ?>">
    <input type="hidden" name="action" value="pluggingg_add_post">
    <?php echo wp_nonce_field('form-submit', '_nonce'); ?>

    <div class="row">
        <div class="col-lg-6">
            <textarea name="commentaire" id="commentaire" cols="30" rows="10" placeholder="commentaire"></textarea>
        </div>
        <div class="col-lg-6">
            <input type="text" name="nom" placeholder="nom" />
            <input type="email" name="courriel" id="courriel" placeholder="adresse courriel" />
            <input type="submit" class="btn" id="submit" value="ENVOYER" />
        </div>
    </div>
</form>