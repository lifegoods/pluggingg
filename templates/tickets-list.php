<?php

namespace Pluggingg\Domain;
use Pluggingg\Domain\Ticket;

if (isset($_GET['nom'])) {
  $nom = $_GET['nom'];
} else{
  $nom = '';
}

?>

<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <title></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="manifest" href="site.webmanifest">
  <link rel="apple-touch-icon" href="icon.png">
  <!-- Place favicon.ico in the root directory -->

  <link rel="stylesheet" href="<?php echo plugins_url("/Pluggingg"); ?>/css/normalize.css">
  <link rel="stylesheet" href="<?php echo plugins_url("/Pluggingg"); ?>/css/bootstrap-grid.css">
  <link rel="stylesheet" href="<?php echo plugins_url("/Pluggingg"); ?>/css/main.css">

  <meta name="theme-color" content="#fafafa">
</head>

<body>
  <header>
    <div class="container">
      <nav>
        <h1 class="logo">Good<span>Good</span></h1>
        <div>
          <a href="tutoriels.html">Tutoriel</a>
          <button class="btn">Dark Mode</button>
        </div>
      </nav>
    </div>
  </header>
  <section>
    <div class="container">
      <div class="row">
        <div class="col-lg-6">

          <?php

            $this->temp = new Ticket();
            $list = $this->temp->get_all_tickets_one_client($nom);

            if($list->have_posts()){
                    while($list->have_posts()){
                        $list->the_post();

                            ?>
                              <div class="card">
                                <div class="card-container">
                                  <?php

                                    $comments = get_comments('post_id='.get_the_ID());
                                    $nb_comment_afficher = 0;
                                    ?>
                                        <h1 class="msg">Ticket : <?php the_ID(); ?></h1>
                                    <?php
                                    foreach ($comments as $comment) {
                                      if ($nb_comment_afficher <= 1){
                                        ?>

                                          <h2> <?php echo $comment->comment_author; ?></h2>
                                            <div class="bubble--yellow">
                                              <p class="txt">
                                                <?php echo $comment->comment_content; ?>
                                              </p>
                                            </div>
                                        <?php
                                      }
                                      $nb_comment_afficher++;
                                    }
                                  ?>

                                    <div class="but-container">
                                      <button class="btn">ÉDITER</button>
                                    </div>
                                </div>
                              </div>
                      <?php

                    }
                ?>
            </select>

          <?php
          }
          ?>

        </div>
      </div>
    </div>
  </section>
  <script src="js/vendor/modernizr-3.7.1.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
  <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.4.1.min.js"><\/script>')</script>
  <script src="js/plugins.js"></script>
  <script src="js/main.js"></script>

  <!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
  <script>
    window.ga = function () { ga.q.push(arguments) }; ga.q = []; ga.l = +new Date;
    ga('create', 'UA-XXXXX-Y', 'auto'); ga('set', 'transport', 'beacon'); ga('send', 'pageview')
  </script>
  <script src="https://www.google-analytics.com/analytics.js" async></script>
</body>

</html>