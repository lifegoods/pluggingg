<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <title></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="manifest" href="site.webmanifest">
  <link rel="apple-touch-icon" href="icon.png">
  <!-- Place favicon.ico in the root directory -->

  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/bootstrap-grid.css">
  <link rel="stylesheet" href="css/main.css">

  <meta name="theme-color" content="#fafafa">
</head>

<body>
  <header>
    <div class="container">
      <nav>
        <h1 class="logo">Good<span>Good</span></h1>
        <div>
          <a href="tutoriels.html">Tutoriel</a>
          <button class="btn">Dark Mode</button>
        </div>
      </nav>
    </div>
  </header>
  <section>
    <div class="container">
    <div class="row">
      <div class="col-lg-6">
      <div class="card">
        <div class="card-container">
          <h1 class="msg">Message : X000</h1>
            <h2>Client : Lorem Ipsum</h2>
              <div class="bubble--yellow">
                <p class="txt">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                In a nunc sit amet felis placerat faucibus ac vitae erat.
                                Sed luctus non nisi tempor finibus. 
                                Nulla vitae bibendum nunc. 
                                Fusce commodo turpis eget elit maximus, id sodales lacus iaculis. 
                                Phasellus volutpat accumsan lacus et faucibus. 
                                Nulla at egestas enim, eu ullamcorper massa.
                </p>
              </div>
            <h2>Entreprise : Lorem Ipsum</h2>
              <div class="bubble--blue">
                <p class="txt">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                In a nunc sit amet felis placerat faucibus ac vitae erat.
                                Sed luctus non nisi tempor finibus. 
                                Nulla vitae bibendum nunc. 
                                Fusce commodo turpis eget elit maximus, id sodales lacus iaculis. 
                                Phasellus volutpat accumsan lacus et faucibus. 
                                Nulla at egestas enim, eu ullamcorper massa.
                </p>
              </div>
              <h2>Entreprise : Lorem Ipsum</h2>
              <form action="">
                <textarea name="" id="" cols="30" rows="10" placeholder="composez votre message"></textarea>
              </form>
            <div class="but-foot">
              <button class="btn">Effacer</button>
              <button class="btn">Éditer</button>
            </div>
        </div>
      </div>
      </div>
  </section>
  <script src="js/vendor/modernizr-3.7.1.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
  <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.4.1.min.js"><\/script>')</script>
  <script src="js/plugins.js"></script>
  <script src="js/main.js"></script>

  <!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
  <script>
    window.ga = function () { ga.q.push(arguments) }; ga.q = []; ga.l = +new Date;
    ga('create', 'UA-XXXXX-Y', 'auto'); ga('set', 'transport', 'beacon'); ga('send', 'pageview')
  </script>
  <script src="https://www.google-analytics.com/analytics.js" async></script>
</body>

</html>