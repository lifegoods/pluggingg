<?php
require __DIR__ . '/vendor/autoload.php';
/**
 * Plugin Name:       Pluggingg
 * Plugin URI:        
 * Description:       Micro CRM.
 * Version:           1.0.0
 * Author:            Louis, Eric, Mandana, Cédric
 * Author URI:        
 * License:           GPL-2.0+
 * Text Domain:       pluggingg
 */
use Pluggingg\Activator;
$activator = new Activator();

$activator->pluggingg_init();