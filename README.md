Nom du plugin : Pluggingg

Shortcode:
------------
------------

Liste les clients et leur email.
[pluggingg_client_list]

------------

Liste les tickets pour un client ( actuellement seulement pour le "Client 1").
[pluggingg_one_client_tickets_listing]

------------

Formulaire ajout d'un ticket.
[pluggingg_ticket_add]

------------

Créer un page Wordpress avec comme fin de permalien (page) : ticketClient
Exemple : : http://siteweb.com/ticketClient/ 